<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $isbn
 * @property string $title
 * @property string $description
 * @property string $created_at
 *
 * @property Reader[] $readers
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isbn', 'title', 'description'], 'required'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['isbn'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isbn' => 'Isbn',
            'title' => 'Title',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReaders()
    {
        return $this->hasMany(Reader::className(), ['book_id' => 'id']);
    }

    /**
     * Возвращает запрос для выборки книг, взятых пользователем.
     * 
     * @param int $userId ID пользователя, для которого нужно получить список книг
     * @return \yii\db\ActiveQuery
     */
    public static function getBooksOnHands($userId)
    {
        return static::find()
            ->from('{{%book}} b')
            ->leftJoin('{{%reader}} r', ['r.book_id' => new \yii\db\Expression('`b`.`id`')])
            ->where(['r.user_id' => (int)$userId, 'r.returned_at' => null])
            ->groupBy(['b.id']);
    }
}
