<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $created_at
 *
 * @property Reader[] $readers
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['created_at'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReaders()
    {
        return $this->hasMany(Reader::className(), ['user_id' => 'id']);
    }

    /**
     * Возвращает запрос для выборки списка самых читающих пользователей.
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function getTop()
    {
        return static::find()
            ->from('{{%user}} u')
            ->leftJoin('{{%reader}} r', ['r.user_id' => new \yii\db\Expression('`u`.`id`')])
            ->groupBy(['u.id'])
            ->orderBy([new \yii\db\Expression('COUNT(`r`.`id`) DESC, `u`.`id` ASC')]);
    }
}
