<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reader".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $book_id
 * @property string $gave_at
 * @property string $returned_at
 *
 * @property Book $book
 * @property User $user
 */
class Reader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reader';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id'], 'required'],
            [['user_id', 'book_id'], 'integer'],
            [['gave_at', 'returned_at'], 'safe'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'book_id' => 'Book ID',
            'gave_at' => 'Gave At',
            'returned_at' => 'Returned At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['user_id'] = function () {
            return isset($this->user_id) ? (int)$this->user_id : null;
        };
        $fields['book_id'] = function () {
            return isset($this->book_id) ? (int)$this->book_id : null;
        };
        return $fields;
    }
}
