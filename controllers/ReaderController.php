<?php

namespace app\controllers;

use yii\rest\ActiveController;

/**
 * Класс для осуществления работы с записями о выдаче/возврате книг.
 * 
 * Доступно только добавление новой записи.
 */
class ReaderController extends ActiveController
{
    public $modelClass = 'app\models\Reader';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'create' => [
                'class' => 'app\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
        ];
    }
}