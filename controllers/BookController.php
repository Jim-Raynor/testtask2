<?php

namespace app\controllers;

use yii\rest\ActiveController;

/**
 * Класс для осуществления работы с книгами.
 * 
 * Доступно только создание новой книги.
 */
class BookController extends ActiveController
{
    public $modelClass = 'app\models\Book';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'create' => [
                'class' => 'app\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
        ];
    }
}