<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\User;
use app\models\Book;
use yii\data\ActiveDataProvider;

/**
 * Класс для осуществления работы с пользователями.
 */
class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'booksOnHands' => ['GET'],
            'topReaders' => ['GET'],
        ];
    }

    /**
     * Возвращает список книг, взятых пользователем.
     * 
     * @param int $id ID пользователя, чей список книг нужно вернуть
     * @return \yii\data\ActiveDataProvider
     */
    public function actionBooksOnHands($id)
    {
        return new ActiveDataProvider([
            'query' => Book::getBooksOnHands($id) ,
        ]);
    }

    /**
     * Возвращает список самых читающих пользователей.
     * 
     * @return \yii\data\ActiveDataProvider
     */
    public function actionTopReaders()
    {
        return new ActiveDataProvider([
            'query' => User::getTop(),
        ]);
    }
}