<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */

    private $bookIsbn;
    private $bookTitle;
    private $bookDescription;

    /**
     * @Given book's isbn is :isbn
     */
    public function booksIsbnIs($isbn)
    {
        $this->bookIsbn = $isbn;
    }

    /**
     * @Given /^book's title is "(.+)"$/
     */
    public function booksTitleIs($title)
    {
        $this->bookTitle = $title;
    }

    /**
     * @Given book's description is
     */
    public function booksDescriptionIs(\Behat\Gherkin\Node\PyStringNode $description)
    {
        $this->bookDescription = $description->__toString();
    }

    /**
     * @When I send request to create book
     */
    public function iSendRequestToCreateBook()
    {
        $data = [
            'isbn' => $this->bookIsbn,
            'title' => $this->bookTitle,
            'description' => $this->bookDescription,
        ];
        $this->dontSeeRecord('app\models\Book', $data);
        $this->sendPOST('books', $data);
    }

    /**
     * @Then I see that new book has been created
     */
    public function iSeeThatNewBookHasBeenCreated()
    {
        $data = [
            'isbn' => $this->bookIsbn,
            'title' => $this->bookTitle,
            'description' => $this->bookDescription,
        ];
        $this->seeRecord('app\models\Book', $data);
        $this->deleteRecord('app\models\Book', $data);
    }

    /**
     * @Given table :table with only rows
     */
    public function emptyTable($table, \Behat\Gherkin\Node\TableNode $rows)
    {
        $this->truncateTable($table);

        $rows = $rows->getRows();
        $keys = array_shift($rows);
        $class = 'app\models\\'.ucfirst(strtolower($table));

        foreach ($rows as $row) {
            $this->haveRecord($class, array_combine($keys, $row));
        }
    }

    /**
     * @When I send request to get readers top
     */
    public function iSendRequestToGetReadersTop()
    {
        $this->sendGET('users/top-readers');
    }

    /**
     * @Then I see that users order equals to :order
     */
    public function iSeeThatUsersOrderEqualsTo($order)
    {
        $this->seeResponseCodeIs(200);
        $this->seeResponseIsJson();

        $expectedUsersOrder = array_map('intval', explode(',', $order));

        $usersOrder = [];
        foreach (json_decode($this->grabResponse(), true) as $row) {
            $usersOrder[] = $row['id'];
        }

        $this->assertSame($expectedUsersOrder, $usersOrder, 'Users order differ');
    }
}
