<?php
namespace Helper;

use Yii;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    /**
     * Удаляет запись из таблицы.
     * 
     * @param string $model
     * @param array $attributes 
     */
    public function deleteRecord($model, $attributes)
    {
        try {
            if ($record = $this->getModule('Yii2')->grabRecord($model, $attributes)) {
                $record->delete();
            }
        } catch (\yii\db\Exception $e) {
            $this->fail("Couldn't delete $model with ".json_encode($attributes)."\nExeption: ".$e->getMessage());
        }
    }

    /**
     * Очищает таблицу.
     * 
     * @param string $table
     */
    public function truncateTable($table)
    {
        try {
            Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0')->execute();
            Yii::$app->db->createCommand()->truncateTable($table)->execute();
        } catch(\yii\db\Exception $e) {
            $this->fail("Couldn't truncate table $table. \nExeption: ".$e->getMessage());
        }
        try {
            Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1')->execute();
        } catch(\yii\db\Exception $e) {
            $this->fail("Couldn't reset foreign checks. \nExeption: ".$e->getMessage());
        }
    }
}
