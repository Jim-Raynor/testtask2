Feature: GetTopReaders
  In order to check readers top
  As an anyone
  I need to get ordered users list

  @slow
  Scenario: get readers top
    Given table "user" with only rows
      | id | firstname | lastname |
      | 1  | John      | Dow      |
      | 2  | Joe       | Stone    |
      | 3  | Keith     | Town     |
      | 4  | Max       | Water    |
      | 5  | Alex      | Car      |
      | 6  | Tom       | Raider   |
      | 7  | Paul      | Nuke     |
    And table "book" with only rows
      | id | isbn    | title            | description |
      | 1  | 111-111 | The book         |             |
      | 2  | 222-222 | Cook book        |             |
      | 3  | 333-333 | Another book     |             |
      | 4  | 444-444 | Book about books |             |
      | 5  | 555-555 | Cook book (2th)  |             |
    And table "reader" with only rows
      | user_id | book_id |
      | 1       | 1       |
      | 2       | 1       |
      | 2       | 2       |
      | 2       | 3       |
      | 2       | 4       |
      | 3       | 1       |
      | 4       | 1       |
      | 5       | 1       |
      | 5       | 2       |
      | 6       | 1       |
    When I send request to get readers top
    Then I see that users order equals to "2,5,1,3,4,6,7"