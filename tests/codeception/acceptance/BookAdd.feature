Feature: BookAdd
  In order to add a book
  As an anyone
  I need to be able to create new book

  Scenario: try to create new book
    Given book's isbn is "234324-888-05"
    And book's title is "John Dow"
    And book's description is
      """
      Some description
      """
    When I send request to create book
    Then I see that new book has been created