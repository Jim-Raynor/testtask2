<?php

namespace app\tests\codeception\fixtures;

use yii\test\ActiveFixture;

class ReaderFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Reader';
    public $depends = [
        'app\tests\codeception\fixtures\BookFixture',
        'app\tests\codeception\fixtures\UserFixture',
    ];
}