<?php
/**
 * Список всех пользователей, с участием которых будут проходить тесты.
 */
return [
    'user1' => [
        'firstname' => 'Chelsea',
        'lastname' => 'Cormier',
        'created_at' => '1986-11-14 00:10:34',
    ],
    'user2' => [
        'firstname' => 'Madilyn',
        'lastname' => 'Wehner',
        'created_at' => '1977-11-13 22:45:07',
    ],
    'user3' => [
        'firstname' => 'Pablo',
        'lastname' => 'Hane',
        'created_at' => '1971-01-17 23:34:38',
    ],
    'user4' => [
        'firstname' => 'Gerson',
        'lastname' => 'McGlynn',
        'created_at' => '1997-08-16 06:55:35',
    ],
    'user5' => [
        'firstname' => 'Rollin',
        'lastname' => 'West',
        'created_at' => '2005-03-15 21:36:27',
    ],
    'user6' => [
        'firstname' => 'Nellie',
        'lastname' => 'VonRueden',
        'created_at' => '1982-09-23 10:07:12',
    ],
    'user7' => [
        'firstname' => 'Darby',
        'lastname' => 'Murazik',
        'created_at' => '1980-07-29 16:24:13',
    ],
    'user8' => [
        'firstname' => 'Yesenia',
        'lastname' => 'Nader',
        'created_at' => '1985-09-22 04:40:18',
    ],
    'user9' => [
        'firstname' => 'Alayna',
        'lastname' => 'Stark',
        'created_at' => '1998-04-27 02:17:23',
    ],
    'user10' => [
        'firstname' => 'Efren',
        'lastname' => 'Fahey',
        'created_at' => '1975-11-13 03:49:04',
    ],
];