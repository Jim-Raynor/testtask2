<?php
/**
 * Список всех книг, с участием которых будут проходить тесты.
 */
return [
    'book1' => [
        'isbn' => '7540557869',
        'title' => 'Magnam quis nihil impedit ut.',
        'description' => 'Ea rerum omnis commodi et dolores. Debitis ut ratione neque quis. Ad enim voluptatem laborum cum possimus et est incidunt.',
        'created_at' => '1996-03-13 22:18:53',
    ],
    'book2' => [
        'isbn' => '5708137509',
        'title' => 'Qui quam ab ea non assumenda quasi quia.',
        'description' => 'Rerum blanditiis adipisci iste ducimus dolores. Asperiores aut rem quod. Sequi quibusdam quam sunt ut rerum. Repellendus adipisci animi porro et explicabo nulla illum.',
        'created_at' => '1971-11-23 22:42:33',
    ],
    'book3' => [
        'isbn' => '6652268302',
        'title' => 'Quia impedit ullam autem voluptas eos eaque est inventore.',
        'description' => 'Ipsa nisi sint natus praesentium et. Rerum sit maxime culpa qui. Nisi expedita odit quaerat molestiae quia dolorem.',
        'created_at' => '2001-05-10 04:09:43',
    ],
    'book4' => [
        'isbn' => '2638040223',
        'title' => 'Soluta numquam delectus et doloribus dolores aut.',
        'description' => 'Consequatur quisquam aut quis et accusantium ratione. Ducimus ipsum provident dicta asperiores ipsum. Eum distinctio voluptas adipisci. Voluptatem et consectetur at voluptatum qui quibusdam ipsa.',
        'created_at' => '1981-12-30 04:54:14',
    ],
    'book5' => [
        'isbn' => '6989562935',
        'title' => 'Quidem non quae sit ut est qui unde.',
        'description' => 'Atque asperiores porro et dolores autem. Dolores pariatur est odit voluptatibus est non voluptates. Pariatur est repellat mollitia est.',
        'created_at' => '1988-01-23 20:44:37',
    ],
    'book6' => [
        'isbn' => '7700774153',
        'title' => 'Accusamus aut reprehenderit et ut qui reiciendis eveniet.',
        'description' => 'Officiis fugit cumque nihil. Qui expedita voluptatibus voluptatem ut explicabo. Nobis reiciendis corrupti omnis optio deleniti.',
        'created_at' => '1983-10-30 01:44:17',
    ],
    'book7' => [
        'isbn' => '4254949480',
        'title' => 'Quis beatae dolorum ab.',
        'description' => 'Nihil possimus in qui et accusamus. Veniam omnis est consequatur officiis odit. Amet dolore qui et deleniti deleniti. Dolor labore et repellendus qui quisquam corporis.',
        'created_at' => '1977-05-05 16:46:19',
    ],
    'book8' => [
        'isbn' => '7566601733',
        'title' => 'Ut debitis sit assumenda quos.',
        'description' => 'Facilis quo deleniti quas incidunt velit distinctio. Ut numquam iure ut sit consequatur.',
        'created_at' => '1975-03-22 21:49:01',
    ],
    'book9' => [
        'isbn' => '3030249603',
        'title' => 'Id autem aut vel dolor.',
        'description' => 'Quibusdam quisquam earum pariatur consequuntur blanditiis occaecati non. Nostrum accusantium quibusdam in eos id officiis. Officia corrupti autem id ea. Aspernatur quos ut aliquid molestias odit.',
        'created_at' => '1982-03-19 04:19:31',
    ],
    'book10' => [
        'isbn' => '1960672762',
        'title' => 'Aut quo officiis perferendis.',
        'description' => 'Accusantium omnis ad aut voluptates quae et. Est quia earum corporis veniam. Sint aperiam nam delectus a distinctio accusantium. Non modi qui dolorem non aut cupiditate.',
        'created_at' => '1996-12-09 02:41:06',
    ],
];