<?php

if (file_exists(__DIR__.'/db.local.php')) {
    $db = require __DIR__.'/db.local.php';
} else {
    $db = [
        'dsn' => 'mysql:host=localhost;dbname=yii2_basic_tests',
    ];
}

/**
 * Application configuration shared by all test types
 */
return [
    'language' => 'en-US',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\faker\FixtureController',
            'fixtureDataPath' => '@tests/codeception/fixtures',
            'templatePath' => '@tests/codeception/templates',
            'namespace' => 'tests\codeception\fixtures',
        ],
    ],
    'components' => [
        'db' => $db,
        'mailer' => [
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
];
