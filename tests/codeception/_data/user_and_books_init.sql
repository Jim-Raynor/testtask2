SET FOREIGN_KEY_CHECKS=0;

TRUNCATE TABLE `book`;
TRUNCATE TABLE `user`;
TRUNCATE TABLE `reader`;

INSERT INTO `book` (`isbn`, `title`, `description`, `created_at`) VALUES
('7540557869', 'Magnam quis nihil impedit ut.', 'Ea rerum omnis commodi et dolores. Debitis ut ratione neque quis. Ad enim voluptatem laborum cum possimus et est incidunt.', '1996-03-13 22:18:53'),
('5708137509', 'Qui quam ab ea non assumenda quasi quia.', 'Rerum blanditiis adipisci iste ducimus dolores. Asperiores aut rem quod. Sequi quibusdam quam sunt ut rerum. Repellendus adipisci animi porro et explicabo nulla illum.', '1971-11-23 22:42:33'),
('6652268302', 'Quia impedit ullam autem voluptas eos eaque est inventore.', 'Ipsa nisi sint natus praesentium et. Rerum sit maxime culpa qui. Nisi expedita odit quaerat molestiae quia dolorem.', '2001-05-10 04:09:43'),
('2638040223', 'Soluta numquam delectus et doloribus dolores aut.', 'Consequatur quisquam aut quis et accusantium ratione. Ducimus ipsum provident dicta asperiores ipsum. Eum distinctio voluptas adipisci. Voluptatem et consectetur at voluptatum qui quibusdam ipsa.', '1981-12-30 04:54:14'),
('6989562935', 'Quidem non quae sit ut est qui unde.', 'Atque asperiores porro et dolores autem. Dolores pariatur est odit voluptatibus est non voluptates. Pariatur est repellat mollitia est.', '1988-01-23 20:44:37'),
('7700774153', 'Accusamus aut reprehenderit et ut qui reiciendis eveniet.', 'Officiis fugit cumque nihil. Qui expedita voluptatibus voluptatem ut explicabo. Nobis reiciendis corrupti omnis optio deleniti.', '1983-10-30 01:44:17'),
('4254949480', 'Quis beatae dolorum ab.', 'Nihil possimus in qui et accusamus. Veniam omnis est consequatur officiis odit. Amet dolore qui et deleniti deleniti. Dolor labore et repellendus qui quisquam corporis.', '1977-05-05 16:46:19'),
('7566601733', 'Ut debitis sit assumenda quos.', 'Facilis quo deleniti quas incidunt velit distinctio. Ut numquam iure ut sit consequatur.', '1975-03-22 21:49:01'),
('3030249603', 'Id autem aut vel dolor.', 'Quibusdam quisquam earum pariatur consequuntur blanditiis occaecati non. Nostrum accusantium quibusdam in eos id officiis. Officia corrupti autem id ea. Aspernatur quos ut aliquid molestias odit.', '1982-03-19 04:19:31'),
('1960672762', 'Aut quo officiis perferendis.', 'Accusantium omnis ad aut voluptates quae et. Est quia earum corporis veniam. Sint aperiam nam delectus a distinctio accusantium. Non modi qui dolorem non aut cupiditate.', '1996-12-09 02:41:06');

INSERT INTO `user` (`firstname`, `lastname`, `created_at`) VALUES
('Chelsea', 'Cormier', '1986-11-14 00:10:34'),
('Madilyn', 'Wehner', '1977-11-13 22:45:07'),
('Pablo', 'Hane', '1971-01-17 23:34:38'),
('Gerson', 'McGlynn', '1997-08-16 06:55:35'),
('Rollin', 'West', '2005-03-15 21:36:27'),
('Nellie', 'VonRueden', '1982-09-23 10:07:12'),
('Darby', 'Murazik', '1980-07-29 16:24:13'),
('Yesenia', 'Nader', '1985-09-22 04:40:18'),
('Alayna', 'Stark', '1998-04-27 02:17:23'),
('Efren', 'Fahey', '1975-11-13 03:49:04');

SET FOREIGN_KEY_CHECKS=1;