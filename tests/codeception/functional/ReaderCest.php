<?php

namespace app\tests\codeception\functional;

use Yii;
use FunctionalTester;

/**
 * @group cest
 */
class ReaderCest
{
    public function _before(FunctionalTester $I)
    {
        Yii::$app->db->createCommand()->truncateTable('reader')->execute();
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function createNewReader(FunctionalTester $I)
    {
        $data = [
            'user_id' => 2,
            'book_id' => 5,
        ];
        $I->sendPOST('readers', $data);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson($data);
    }

    public function createNewReaderFails(FunctionalTester $I)
    {
        foreach ($this->_incorrectDataProvider() as $descr => $row) {
            $I->amGoingTo('Create new reader - '.$descr);

            $I->sendPOST('readers', $row[0]);
            $I->seeResponseCodeIs(422);
            $I->seeResponseIsJson();
            $I->seeResponseContainsJson($row[1]);
        }
    }

    public function _incorrectDataProvider()
    {
        return [
            'without:book_id' => [
                ['user_id' => 2],
                [['field' => 'book_id']]
            ],
            'without:user_id' => [
                ['book_id' => 2],
                [['field' => 'user_id']]
            ],
            'wrong:user_id' => [
                ['user_id' => 'string', 'book_id' => 3],
                [['field' => 'user_id']]
            ],
            'foreignKeyRestriction:user_id' => [
                ['user_id' => 77, 'book_id' => 3],
                [['field' => 'user_id']]
            ],
            'foreignKeyRestriction:book_id' => [
                ['user_id' => 2, 'book_id' => 100],
                [['field' => 'book_id']]
            ],
        ];
    }
}