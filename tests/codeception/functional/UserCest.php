<?php

namespace app\tests\codeception\functional;

use Yii;
use FunctionalTester;

/**
 * @group cest
 */
class UserCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function getTopReaders(FunctionalTester $I)
    {
        Yii::$app->db->createCommand()->truncateTable('reader')->execute();
        $fixture = require dirname(__DIR__).'/fixtures/data/readers_top.php';
        Yii::$app->db->createCommand()->batchInsert('reader', array_keys($fixture[0]), $fixture)->execute();

        $I->sendGET('users/top-readers');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        
        $expectedUsersOrder = [
            0 => 7,
            1 => 1,
            2 => 4,
            3 => 5,
            4 => 2,
            5 => 9,
            6 => 3,
            7 => 6,
            8 => 8,
            9 => 10,
        ];

        $usersOrder = [];
        foreach (json_decode($I->grabResponse(), true) as $row) {
            $usersOrder[] = $row['id'];
        }
        $I->assertSame($expectedUsersOrder, $usersOrder, 'Users order differ');
    }
}
