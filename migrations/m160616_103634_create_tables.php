<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tables`.
 */
class m160616_103634_create_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%book}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID книги в системе'),
            'isbn' => $this->string(20)->notNull()->comment('isbn книги'),
            'title' => $this->string(255)->notNull()->comment('Название книги'),
            'description' => $this->text()->notNull()->comment('Краткое описание книги'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()')->comment('Время добавления книги'),
        ], $tableOptions);
        $this->addCommentOnTable('{{%book}}', 'Книги, существующие в системе');

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID пользователя в системе'),
            'firstname' => $this->string(100)->notNull()->comment('Имя пользователя'),
            'lastname' => $this->string(100)->notNull()->comment('Фамилия пользователя'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()')->comment('Время добавления пользователя'),
        ], $tableOptions);
        $this->addCommentOnTable('{{%user}}', 'Пользователи системы');

        $this->createTable('{{%reader}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID записи о выдаче/возврате книги'),
            'user_id' => $this->integer(11)->unsigned()->notNull()->comment('ID пользователя, взявшего книгу'),
            'book_id' => $this->integer(11)->unsigned()->notNull()->comment('ID взятой книги'),
            'gave_at' => $this->timestamp()->notNull()->defaultExpression('NOW()')->comment('Время выдачи книги'),
            'returned_at' => $this->timestamp()->comment('Время возврата книги'),
        ], $tableOptions);
        $this->addCommentOnTable('{{%reader}}', 'История всех выдач и возвратов книг пользователям');
        $this->createIndex('idx__user_id', '{{%reader}}', 'user_id');
        $this->createIndex('idx__book_id', '{{%reader}}', 'book_id');
        $this->addForeignKey('fk__user__id', '{{%reader}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk__book__id', '{{%reader}}', 'book_id', '{{%book}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%reader}}');
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%book}}');
    }
}
