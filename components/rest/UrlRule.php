<?php

namespace app\components\rest;

/**
 * @inheritdoc
 */
class UrlRule extends \yii\rest\UrlRule
{
    /**
     * Проверяется в Codeception при включенном prettyUrl, но в оригинальном
     * \yii\rest\UrlRule этого поля нет, что приводит к исключению.
     */
    public $host = null;
}
